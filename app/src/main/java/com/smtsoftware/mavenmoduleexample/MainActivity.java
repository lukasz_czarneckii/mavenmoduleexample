package com.smtsoftware.mavenmoduleexample;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.smtsoftware.common.android.imageloader.ImageLoader;
import com.smtsoftware.common.android.imageloader.impl.FrescoImageLoader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(R.layout.activity_main);
        final ImageLoader imageLoader = new FrescoImageLoader(this);
        final ImageView imageView = (ImageView) findViewById(R.id.image_view);
        imageLoader.loadImage("https://media.licdn.com/mpr/mpr/p/6/005/087/2b2/09f6f94.jpg", imageView);
    }
}
